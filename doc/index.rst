.. ptychoSTEM documentation master file, created by
   sphinx-quickstart on Sat Dec 14 18:58:37 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ptychoSTEM's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Example data can be found here: https://zenodo.org/record/3578284/files/m.mat?download=1


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
